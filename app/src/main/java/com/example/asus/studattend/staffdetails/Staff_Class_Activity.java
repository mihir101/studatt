package com.example.asus.studattend.staffdetails;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.asus.studattend.CustomeBase_adapter;
import com.example.asus.studattend.Model_class;
import com.example.asus.studattend.R;
import com.example.asus.studattend.studentdetails.Studregi;

import java.util.ArrayList;

public class Staff_Class_Activity extends AppCompatActivity {
TextView addstudent;
ListView standardlist;

ArrayList<Model_class> arrayList;

CustomeBase_adapter customeBase_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staff__class_);

        addstudent=findViewById(R.id.addstudent);
        standardlist=findViewById(R.id.standardlist);



        //intent to student registration
        addstudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(Staff_Class_Activity.this,Studregi.class);
                startActivity(i);
            }
        });

        //adding data to list

        ArrayList<Model_class>dataset=new ArrayList<>();

        dataset.add(new Model_class("First Year","500"));
        dataset.add(new Model_class("Second Year","500"));
        dataset.add(new Model_class("Third Year","500"));

       CustomeBase_adapter a=new CustomeBase_adapter(dataset,getApplicationContext());
       standardlist.setAdapter(a);

        standardlist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (position==0)
            {

            }
            if (position==1)
            {

            }
            if (position==2)
            {

            }

            }
        });
    }
}
