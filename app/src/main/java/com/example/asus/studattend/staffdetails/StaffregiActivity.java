package com.example.asus.studattend.staffdetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.Toast;

import com.example.asus.studattend.R;

public class StaffregiActivity extends AppCompatActivity {
EditText sid,name,email,mobile,address,city,pincode,username,password;
RadioButton male,female;
Button save;
    String pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_staffregi);

        //validation

       sid=findViewById(R.id.sid);
        name=findViewById(R.id.name);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        address=findViewById(R.id.address);
        city=findViewById(R.id.city);
        pincode=findViewById(R.id.pin);
        username=findViewById(R.id.username);
        male=findViewById(R.id.rdmale);
        female=findViewById(R.id.rdfemale);
        save=findViewById(R.id.save);



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sid.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter SID", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (name.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter Name", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (email.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter Email", Toast.LENGTH_SHORT).show();
                    return;
                }
               else if (!email.getText().toString().matches(pattern))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter Valid Email", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (mobile.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter Mobile", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (address.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter Address", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (city.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter City", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (pincode.getText().toString().equals(""))
                {
                    Toast.makeText(StaffregiActivity.this, "Enter Pincode", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
        });
    }
}
