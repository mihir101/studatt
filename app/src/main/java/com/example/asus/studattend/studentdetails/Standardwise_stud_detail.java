package com.example.asus.studattend.studentdetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.asus.studattend.Custombase_list;
import com.example.asus.studattend.CustomeBase_adapter;
import com.example.asus.studattend.Model_class;
import com.example.asus.studattend.R;

import java.util.ArrayList;

public class Standardwise_stud_detail extends AppCompatActivity {
ListView studdetails;
    ArrayList<Model_class> arrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_standardwise_stud_detail);
        studdetails=findViewById(R.id.studdetails);


        ArrayList<Model_class>dataset=new ArrayList<>();

        dataset.add(new Model_class(R.drawable.ic_launcher_foreground,1,"mihir","p"));
        dataset.add(new Model_class(R.drawable.doveimage,2,"nirlai","p"));
        dataset.add(new Model_class(R.drawable.doveimage,3,"pooja`","p"));
        dataset.add(new Model_class(R.drawable.doveimage,4,"mihir","p"));


        Custombase_list a=new Custombase_list(dataset,getApplicationContext());
        studdetails.setAdapter(a);
/*
        Custombase_list a=new Custombase_list(dataset1,getApplicationContext());
        studdetails.setAdapter(a);*/

    }
}
