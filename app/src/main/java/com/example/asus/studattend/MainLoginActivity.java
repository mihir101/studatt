package com.example.asus.studattend;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainLoginActivity extends AppCompatActivity {
    EditText userid, pass;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        userid = findViewById(R.id.userid);
        pass = findViewById(R.id.userpass);
        submit = findViewById(R.id.submit);


        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //validation
                if (userid.getText().toString().equals("")) {

                    Toast.makeText(MainLoginActivity.this, "Enter UserId", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (pass.getText().toString().equals("")) {

                    Toast.makeText(MainLoginActivity.this, "Enter Password", Toast.LENGTH_SHORT).show();
                    return;
                }

            }
        });

    }
}
