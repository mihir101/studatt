package com.example.asus.studattend;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class CustomeBase_adapter extends ArrayAdapter<Model_class> {

    ArrayList<Model_class> arrayList = new ArrayList<>();

    Context context;

    public CustomeBase_adapter(ArrayList<Model_class> dataset,Context context) {
        super(context, R.layout.activity_standard__list__item,dataset);

        arrayList = dataset;
        this.context=context;

    }
//for activity_standard__list__item
    private static class Viewho {


        TextView fy;
        TextView txtfy;

    }


    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
        //return super.getView(position, view, parent);
        Viewho vh;
           final View result;

        if (view==null)
        {
            vh=new Viewho();
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.activity_standard__list__item,parent,false);
            vh.fy=(TextView)view.findViewById(R.id.total);
            vh.txtfy=(TextView)view.findViewById(R.id.txtfy);



            result=view;
            view.setTag(vh);

        }
        else
        {
            vh=(Viewho)view.getTag();
            result=view;
        }
        Model_class i=getItem(position);
        vh.fy.setText(arrayList.get(position).getStandard());
        vh.txtfy.setText(arrayList.get(position).getTotal());


        return view;
    }



}
