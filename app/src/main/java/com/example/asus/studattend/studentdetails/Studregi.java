package com.example.asus.studattend.studentdetails;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.asus.studattend.R;

public class Studregi extends AppCompatActivity {
TextView sid,roll,studname,standard,division,email,mobile,dob,address,city,pin,username,pass;
    RadioButton male,female;
    Button save;
    String pattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studregi);

        sid=findViewById(R.id.sid);
        roll=findViewById(R.id.roll);
        studname=findViewById(R.id.studname);
        standard=findViewById(R.id.standard);
        division=findViewById(R.id.division);
        email=findViewById(R.id.email);
        mobile=findViewById(R.id.mobile);
        dob=findViewById(R.id.dob);
        address=findViewById(R.id.address);
        city=findViewById(R.id.city);
        pin=findViewById(R.id.pincode);
        username=findViewById(R.id.username);
        pass=findViewById(R.id.pass);
        male=findViewById(R.id.rdmale);
        female=findViewById(R.id.rdfemale);
        save=findViewById(R.id.save);



        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sid.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Student ID", Toast.LENGTH_SHORT).show();
                }

                if (roll.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Student Roll no", Toast.LENGTH_SHORT).show();
                }

                if (standard.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Student standard", Toast.LENGTH_SHORT).show();
                }

                if (division.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Student division", Toast.LENGTH_SHORT).show();
                }

                if (email.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Student email", Toast.LENGTH_SHORT).show();
                }else if(!email.getText().toString().matches(pattern))
                {
                    Toast.makeText(Studregi.this, "Enter Valid Email", Toast.LENGTH_SHORT).show();
                }

                if (mobile.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Father's number", Toast.LENGTH_SHORT).show();
                }

                if (dob.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Student DOB", Toast.LENGTH_SHORT).show();
                }

                if (address.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Address", Toast.LENGTH_SHORT).show();
                }

                if (city.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter City", Toast.LENGTH_SHORT).show();
                }

                if (pin.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Pincode", Toast.LENGTH_SHORT).show();
                }

                if (username.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter USer ID", Toast.LENGTH_SHORT).show();
                }

                if (pass.getText().toString().equals(""))
                {
                    Toast.makeText(Studregi.this, "Enter Password", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
}
