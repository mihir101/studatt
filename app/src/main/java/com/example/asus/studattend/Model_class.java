package com.example.asus.studattend;

public class Model_class {
    //for staff_class_activity
    public String standard;
    public String total;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    //constructor for studentwise_detail
    public Model_class(int profilepic, int rollno, String sname,String status) {
        this.profilepic = profilepic;
        this.rollno = rollno;
        this.sname = sname;
        this.status = status;
    }

    public int profilepic;
    public int rollno;
    public String sname;
    public String status;

//constructor for standardwise_details
    public Model_class(String standard, String total) {
        this.standard = standard;
        this.total = total;
    }
    //getter and setter
    public int getProfilepic() {
        return profilepic;
    }

    public void setProfilepic(int profilepic) {
        this.profilepic = profilepic;
    }

    public int getRollno() {
        return rollno;
    }

    public void setRollno(int rollno) {
        this.rollno = rollno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }


    public String getStandard() {
        return standard;
    }

    public void setStandard(String standard) {
        this.standard = standard;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }
}
