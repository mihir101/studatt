package com.example.asus.studattend;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

public class Custombase_list extends ArrayAdapter<Model_class> {

    ArrayList<Model_class> arrayList = new ArrayList<>();

    Context context;


    public Custombase_list(ArrayList<Model_class> dataset,Context context) {
        super(context, R.layout.standardwisestud_details_list_item,dataset);
        arrayList = dataset;
        this.context=context;

    }

    //for standardwisestud_details_list_item.xml
    private static class viewholder{

        ImageView studprofilepic;
        TextView rollno;
        TextView studentname;
        TextView status;


    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View view, @NonNull ViewGroup parent) {
       // return super.getView(position, view, parent);
        viewholder vh1;
        final View result;
        if (view==null)
        {
            vh1=new viewholder();
            view= LayoutInflater.from(parent.getContext()).inflate(R.layout.standardwisestud_details_list_item,parent,false);
            vh1.studprofilepic=(ImageView) view.findViewById(R.id.studprofilepic);
            vh1.rollno=(TextView)view.findViewById(R.id.roll);
            vh1.studentname=(TextView)view.findViewById(R.id.studentname);
            vh1.status=(TextView)view.findViewById(R.id.status);


            result=view;
            view.setTag(vh1);

        }
        else
        {
            vh1=(viewholder)view.getTag();
            result=view;
        }
        Model_class i=getItem(position);
        Glide.with(context)
                .load(arrayList.get(position).getProfilepic())
                .into(vh1.studprofilepic);
        //vh.studprofilepic.setText(arrayList.get(position).getStandard());
        vh1.rollno.setText(arrayList.get(position).getRollno()+"");
        vh1.studentname.setText(arrayList.get(position).getSname());
        vh1.status.setText(arrayList.get(position).getStatus());



        return view;
    }
}
